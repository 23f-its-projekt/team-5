# Problemformulering

**Emne:** System og netværk

**Problem felt:** IT-sikkerhed

**Problemstilling:** sikre mod databrud

**Problemformulering:** Hvordan kan SKI mindske risikoen for databrud? 

___

**Hypotese:** En netværksopsætning hvor at eksterne aktører ikke kan skaffe sig adgang til hele SKI’s netværk vil højne sikkerheden

- **Eksperiment 1:** Opsætte et virtuelt netværk som skal være en abstrakt udgave af SKI’s netværksopsætning. Test om en ekstern aktør kan få adgang til SKI’s netværk.

	- **Kriterie:** Man har anskaffet sig brugerrettigheder fra en enhed på netværket 

- **Eksperiment 2:** Tillad en ekstern aktør adgang til en del af SKI’s netværk. Lad personen teste om de kan skaffe sig adgang til andre dele af netværket.

	- **Kriterie:** Tjek ip adressen om der er blevet tilkoblet til et nyt subnet

___

**Hypotese:** En systemopsætning hvor at interne aktører kun har adgang til det der er nødvendigt for dem selv vil højne sikkerheden

- **Eksperiment:** lade en intern aktør som kun har prædefinerede rettigheder teste, om de kan gøre ting som de ikke burde have rettigheder til

	- **Kriterie:** En fil som aktøren ikke burde have rettigheder til er blevet ændret eller slettet 
___

**Hypotese:** Indførelse af monitorering vil resultere i at virksomheden hurtigt kan agere i tilfælde af potentielt databrud

- **Eksperiment:** Laver en øvelse hvor man med vilje sætter en alarm i gang, hvorefter man tester tiden det tog før de kan identificere hændelsen baseret på alarmens parametre 

	- **Kriterie:** Man skal have nået at identificere hændelsen inden for X antal tid

Evt lav eksperiment hvor vi tester et systems prækonfigurerede reaktion når en alarm går i gang 

