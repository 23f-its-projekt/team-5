## Projekt beskrivelse

Projektets udgangspunkt er en virksomhed med flere afdelinger.
Virksomheden kan være fiktiv eller en rigtig virksomhed, det beslutter i selv i jeres team.

Virksomheden skal have minimum 5 afdelinger, eksempler på afdelinger kan være:

- Regnskab
- Software udvikling
- Lager
- Service og support
- IT drift


I de afdelinger skal i afdække hvilke enheder og services de forskellige afdelinger benytter.

Eksempler på enheder kan være:

- Bruger computere
- Servere
- Printere
- Håndscannere
- Switche
- Routere

Eksempler på services kan være:

- Active directory
- IP telefoni
- VPN til hjemmearbejde
- Webshop
- Server virtualisering
- Regnskabsprogram
- CRM (customer relation management system)


Listerne herover er ikke udtømmende og det er jeres opgave at beskrive en virksomheds aktiver på en realistisk måde.

I kan som nævnt basere det på en fiktiv virksomhed som i selv opfinder, i kan også opsøge en virksomhed og spørge om de vil være en del af jeres projekt.
Det kan også være at i har studiejob i en virksomhed i kan bruge i projektet.

Det er ikke realistisk at i kan bygge en model af hele virksomheden i f.eks. vmware, men i kan lave isolerede setups der demonstrerer forskellige tiltag der vedrører sikkerhed.

I bør inddrage de opgaver i laver i de enkelte fag som en del af projektets sikkerhedstiltag, herunder overvågning, logindsamling, fejlfinding, konfigurationer etc.

## Krav til projektet

- Projektet skal dække væsentlige læringsmål fra hvert fag (Se studieordningen).
- Netværk og andre systemer skal dokumenteres med passende diagrammer.
- Alle teams skal deltage i planlagte milepæls aktiviteter.
- Alt kode, dokumentation o.l. skal løbende dokumenteres i et offentlig gitlab projekt, placeret i gruppen [https://gitlab.com/23f-its-projekt](https://gitlab.com/23f-its-projekt) 
- Der skal udarbejdes en projektplan. Projektplanen skal udformes som readme.md i jeres gitlab projekt. Projektplanen **skal** indeholde hvad der er beskrevet i [projectplan_template.md](https://gitlab.com/23f-its-projekt/23f-its-project-description/-/blob/main/pages/docs/projektplan_template.md) samt et overblik der kan forstås af beslutningstagere i virksomheden (CEO, CTO etc.)
- Projektet skal indeholde udvikling af et eller flere tekniske sikkerheds tiltag (netlab er en mulighed)
- Projektet skal inkludere sikkerheds test af de(t) tekniske produkt(er) som i udvikler
- Projektetplanen skal godkendes af de ansvarshavende undervisere (Se milepæls aktiviteter)
- Rapporten der afleveres til eksamination i _System sikkerhed_ skal udarbejdes som et mini bachelor projekt og følge: 
*Formalia for de merkantile uddannelser på UCL* [https://esdhweb.ucl.dk/D22-2039642.pdf](https://esdhweb.ucl.dk/D22-2039642.pdf)
- Til håndtering af referencer i rapporter bør i benytte zotero, vejledning er her: [https://sites.google.com/ucl.dk/zotero-ucl/startside](https://sites.google.com/ucl.dk/zotero-ucl/startside) og biblioteket på seebladsgade kan også være behjælplige med zotero.

## Eksaminering
For hvert af de 3 fag er der en prøve jvf. afsnit 5.2 i den [instituitionelle del af studioordningen](https://esdhweb.ucl.dk/D22-1972441.pdf). 

Hvis et semester eksempelvis består af følgende fag:  

- Introduktion til IT-sikkerhed 
- System sikkerhed
- Netværks- og kommunikationssikkerhed

Hvert af de 3 fag vil skulle afsluttes med en prøve.  
Prøveformen for hvert fag er beskrevet i den institutionelle studieordning.  

Eksempelvis kan et fags prøveform være beskrevet således:    

_Prøven er en individuel, mundtlig prøve med udgangspunkt i et spørgsmål, som den studerende trækker til eksamen._  
_Alle spørgsmål, der kan trækkes til eksamen, er udleveret til de studerende senest 14 dage før eksamen, så de studerende har mulighed for at forberede sig._  
**Hvad angår spørgsmålene, vil der blive lagt vægt på, at den studerende kan inddrage eksempler fra projektarbejdet og praktiske øvelser fra det forgangne semester.**  
_Der er ingen forberedelse på selve dagen._      

I dette konkrete tilfælde vil det betyde at de eksempler som inddrages i eksamination er taget fra semester projektet.  

Et andet eksempel kan være at der inden den mundtlige eksamination, skal afleveres en rapport.  
Her vil rapporten skulle tage udgangspunkt i dele af projektet.

**Husk:**  
- Altid at orientere dig om den enkelte eksamen i den institutionelle studieordning.  
- Altid rådføre dig med underviseren i det enkelte fag ved tvivlsspørgsmål.  

## Milepæls aktiviteter (milestones)

Milepæls aktiviteter har til formål at støtte de studerende i løbet af deres projekt proces, samt at give underviserne indblik i de studerendes arbejde med projektet. 
På den måde skabes grundlag for vejledning og støtte.

**Alle milepæls aktiviteterne er obligatoriske for alle teams.** 

Projektet har 4 milepæle med deltagelse af underviser. I bør oprette yderligere milepæle i jeres team, f.eks med et 14. dages interval.

Milepæls aktiviteterne **må ikke misforstås som en opfordring til at arbejde ud fra en waterfall metodologi.**  
Vi opfordrer til at arbejde agilt (særligt når undervisningen foregår parallelt med projektet). 

Milepls aktiviteterne med underviser er skemalagt i ugerne 8, 11, 16 og 18.

I de følgende underafsnit er der en forklaring på hver milepæls aktivitet.

### 1. Præsentation af use cases og foreløbig projekt arbejde for klassen

I denne aktivitet skal teamet udarbejdes en ide til projektet samt en projektplan.

Projektplanen er defineret og skabelonen til projektplanen finder i her:

[projectplan_template.md](https://gitlab.com/23f-its-projekt/23f-its-project-description/-/blob/main/pages/docs/projektplan_template.md)

Husk at projektplanen skal være en del af jeres readme.md fil på gitlab.

### 2, 3 og 4. Vejledning af teams med deltagelse af undervisere

Indholdet i disse 3 milepæle er ens og består af vejledning fra undervisere til teams. Hvert team har 30 minutters vejledning hvor der gives feedback på det arbejde teamet har udført indtil videre samt svar og råd i forhold til projektets næste skridt.  

For at få så meget ud af vejledningen som muligt er det vigtigt at i forbereder en dagsorden til mødet. 

Det er selvfølgelig også muligt at få vejledning uden for disse milepæle, for eksempel i undervisningen i de enkelte fag.
