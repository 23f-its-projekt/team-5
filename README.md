### **Team Fünf** - Camilla, Signe & Lars
[Link til GitPages](https://23f-its-projekt.gitlab.io/team-5/)
# Projektplan

## Baggrund

*Hvad er baggrunden for projektet*

Baggrunden for projektet er at vi som studerende får indsigt i hvordan en virksomheds struktur kan se ud, og hvilke indgangsvinkler der kan opstå med IT-Sikkerhed i fokus. 

## Formål

*Hvad er formålet med projektet*

Formålet er at vi har et godt grundlag til vores eksamener, og at vi kan vise vores læringsforløb samt tekniske færdigheder. 

## Mål

*Projektet leverer følgende*

Projektet leverer en rapport, samt git page hvor al fremskridt bliver dokumenteret af hvert team medlem.

*Version 4. maj*

Vi bruger et eller flere værktøjer vi har lært fra undervisningen. 

- SIEM-system
- Brug af CLI

Vi vil gerne vise at vi har opnået god teoretisk og praktisk viden omkring de forskellige emner fra undervisningen.

- Opsætning af virtuelt miljø herunder netværk, enheder samt installation af software
- Opsætning af logs
- Opsætning af SIEM-system
- CIS kontroller
- Adgangskontrol
- Trusselsmodellering

Hvert gruppemedlem har fuld forståelse for alle aspekter af projektet. 

- Hvert gruppemedlem skal forstå og kunne snakke om hvert punkt i rapporten.

Vi leverer en rapport der overholder nedenstående krav.
- Opsætning af litteraturlister med Zotero
- [Formalia krav til rapport](https://esdhweb.ucl.dk/D22-2039642.pdf)
- Hvert gruppemedlem læser den færdige rapport igennem en ekstra gang inden aflevering med fokus på korrekturlæsning. 

### Nice to have

1. Git Pages
2. Dokumentation rettet mod virksomheden
3. ..

### Need to have

1. Rapport til System Sikkerhed
2. Brug af issueboard til overblik over projekt
3. ..
___
## Tidsplan

Agenda:

- Hvordan har teamarbejdet været i ugen?
- Update på ugens forløb (Gitpages, evt. andet projektarbejde, Issue-board)
- Aftale dage i næste uge (Booke lokaler)

### Uge 8

- [x] Projektider
- [x] Projektplan
- [x] Lave gitpages
- [x] Sætte noget igang med en virksomhed
- [x] Booke lokaler
- [x] Lave spørgeskema

**Projekttider**

- Torsdag d. 23. - 08:15 - 12:30 (A 3.53)
- Fredag d. 24. - 08:15 - Fredagsbar (A 1.107)

### Uge 11

- [x] Møde med *SKI (Staten og Kommunernes Indkøbsservice A/S)*
- [x] Renskrive noter fra mødet
- [x] Formulere spørgsmål til Andreas

**Projekttider**

- Onsdag d. 15. - 11:30 - 14:30 (B 0.41)

### Uge 12

* [x] Vejledningsmøde
* [ ] Booke lokaler til næste uge

### Uge 13
* [x] Dannet os et overblik over firmaets arkitektur
* [x] Uddeligere opgaver
* [ ] Planlægge næste møde

**Projekttider**

- Torsdag d. 30. marts kl 11:30

**Opgaver:**

Uge 14 er påskeferie. 
I påskeferien kigger hvert teammedlem på alle services og evt. sårbarheder.

### Uge 15

**Projekttider**

- Onsdag d. 12. april kl. 11:30 - 15:30 (A 3.53)

**Opgaver:**

* [x] Problemformulering
* [x] Forberedelse til møde m. undervisere
* [x] Uddelligere opgaver


### Uge 16

**Projekttider**

**Opgaver**

### Uge 17

**Projekttider**

- Tirsdag d. 25/4

**Opgaver**
- * [ ] Der skal udarbejdes et fysisk netværksdiagram over virksomheden
- * [x] Der skal laves en indholdsfortegnelse over hvad der skal være i rapporten
- * [x] Smid ting ind på Gitlab
- - [ ] Lav systemmål og trusselsmodellering

### Uge 18

Møde med undervisere mandag d. 1/5.

### Uge 19

Find ud af hvilke maskiner der skal bruges til virtualisering.
Opsæt virtuelle maskiner. 

### Uge 24
- Aflever rapport!
___
## Organisation

*Hvem er medlemmerne i teamet og hvad er deres roller i projektet.*

**Camilla** (2. semester): Opstarter, Kontaktskaber & Formidler

**Signe** (1. Semester): Organisator, Afslutteren & Analysator

**Lars** (1. Semester): Organisator, Analysator

## Risikostyring

![1/2](pages/docs/Risikoanalyse1.jpg)
![2/2](pages/docs/Risikoanalyse2.jpg)

## Interessenter

Her skrives resultatet af interessentanalyse


|  | Ikke indflydelse på projektet |Indflydelse på |
| ----------- | ----------- |---|
| **Påvirket af projektet** | Virksomheden |Team Fünf |
| **Ikke påvirket af projektet** |  | UCL |

## Kommunikation

Vi arbejder og kommunikere via. nedennævnte kanaler.

### Kommunikationskanaler

- Gitlab issue board
- Element
- Discord
- Email adresser
    - cbka50714@edu.ucl.dk
    - sjjo26454@edu.ucl.dk
    - lhve53799@edu.ucl.dk

### Kommunikationsaktiviteter

Vi har faste møder hver uge, hvor vi i slutningen af ugen planlægger den næste uge, booker lokaler osv. 

## Perspektiv

Projektet danner grundlag for uddannelsens øvrige semesterprojekter og skal opfattes som en øvelse i at lave et godt bachelor projekt som afslutning på uddannelsen.

## Evaluering

Hvordan evalueres projektet udover de obligatoriske eksamens afleveringer.  
Det anbefales at evaluere alle projekter ved afslutning for at synliggøre og samle erfaringer til brug i næste projekt.  
Det er også en god ide at, som team, at kigge tilbage på projektets forløb for at reflektere over hvad de enekelte teammedlemmer har lært i løbet af projektet.

Endelig bør et godt udført og veldokumenteret projekt inkluderes i en studerendes portfolio/dokumentation side så det er muligt for andre at få glæde af projeket.

## Referencer

Relevante overordnede referencer til essentielle ting der benyttes i projektet, hver reference bør have en beskrivelse der kort begrunder og forklarer hvordan referencer er relevant for projektet.

- Links til dokumentation
- Links til metoder/værktøjer
- mm.


