# Netværksdiagram

![netværksdiagram](netværksdiagram.png)

## Virtualisering


![virtualisering](virtualisering.png)

Dette er vores scope i forhold til opsætning i VMWare.

2 x Ubuntu 22.04 på hvert sit subnet (IT - vmnet 111 & Økonomi - vmnet112)
1 x OpenBSD router

Kig på CIS Benchsmarks: *opdateres*

- 3.5.3.1 iproutes firewall
- 3.5.1.3 updates

